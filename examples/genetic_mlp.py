"""Optimize the weights of a Multi Layer Perceptron using a genetic
algorithm."""
from gradient_free_rl.optimizers import GeneticOptimizer
from gradient_free_rl.policies import MLPPolicy
from gradient_free_rl.spaces import Space
from gradient_free_rl.logging.pixels import pixel_arrays_to_video
import gym


def env_maker():
    return gym.make("MountainCarContinuous-v0")


if __name__ == "__main__":
    # Create a multi-layer perceptron and a genetic optimizer
    observation_space = Space.from_gym(env_maker().observation_space)
    action_space = Space.from_gym(env_maker().action_space)
    policy = MLPPolicy.from_uniform(
        inner_sizes=[4, 4, 4, 4, 4],
        activation="tanh",
        mutation_stdev=0.1,
        observation_space=observation_space,
        action_space=action_space,
    )
    optimizer = GeneticOptimizer(
        mutated_n=1000,
        crossover_n=1000,
        make_env=env_maker,
        rollout_len=100,
        rollout_n=10,
        elite_n=5,
    )

    # Iterate the optimization algorithm
    policy = optimizer.optimized(policy, timesteps=1e7)

    # Optimization is done. Visualize results.
    env = env_maker()
    obs = env.reset()
    pixels = list()
    for i in range(2000):
        action = policy(obs)
        obs, reward, done, info = env.step(action)
        pixels.append(env.render("rgb_array"))
        if done:
            obs = env.reset()
    pixel_arrays_to_video(pixels, "video.mp4", 60)

    # Serialization is trivial, as all policies are JSON serializable
    with open("file.json", "wt") as fp:
        fp.write(policy.json)

    with open("file.json", "rt") as fp:
        policy2 = MLPPolicy.from_json("\n".join(fp))

    assert policy.mlp == policy2.mlp
