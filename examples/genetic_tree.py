"""Benchmarking a simple genetic algorithm in control tasks, optimizing the
predicates and leafs of decision trees from scratch."""
from gradient_free_rl.optimizers import GeneticOptimizer
from gradient_free_rl.policies import ConstantPolicy
from gradient_free_rl.predicates import ThresholdPredicate
from gradient_free_rl.policies import DecisionTreePolicy
from gradient_free_rl.spaces import Space
import pybullet_envs
import gym
# Doing manual recording because gym.wrappers.Monitor hangs up on line 307
from PIL import Image
import ffmpeg
import concurrent.futures
from pathlib import Path
import shutil

fps = 60.0
output_dir = Path("movie")

def env_maker():
    return gym.make("BipedalWalker-v3")

def get_filename(i: int) -> Path:
    idstr = str(i).rjust(20, "0")
    return (output_dir/idstr).with_suffix(".png")

observation_space = Space.from_gym(env_maker().observation_space)
action_space = Space.from_gym(env_maker().action_space)

# Our class of leaf nodes is the class of Multi Layer Perceptrons
def policy_maker():
    return ConstantPolicy(
        stdev=0.01,
        action_space=action_space,
    )

# Our class of predicates is the class of single-output Multi Layer
# Perceptrons
def predicate_maker():
    return ThresholdPredicate(
        observation_space=observation_space,
    )


if __name__ == "__main__":
    if output_dir.exists():
        shutil.rmtree(output_dir)
    output_dir.mkdir()

    # Our class of policies
    policy = DecisionTreePolicy(
        max_height=10,
        policy_maker=policy_maker,
        predicate_maker=predicate_maker,
    )
    print(policy)

    optimizer = GeneticOptimizer(
        population_size=1000,
        make_env=env_maker,
        rollout_len=10,
        elite_n=10,
    )
    policy = optimizer.optimized(policy, timesteps=1e7)

    env = env_maker()
    obs = env.reset()
    futures = list()
    filenames = list()
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for i in range(1000):
            action = policy(obs)
            obs, reward, done, info = env.step(action)
            frame = env.render(mode="rgb_array")
            image = Image.fromarray(frame)
            filename = get_filename(i)
            filenames.append(filename)
            futures.append(executor.submit(image.save, filename))
            if done:
                obs = env.reset()

    # Concatenate images into mp4 movie
    for future in futures:
        future.result()
    filename = (output_dir/"movie").with_suffix(".mp4")
    (
            ffmpeg
            .input(output_dir/"*.png", pattern_type="glob", framerate=fps)
            .output(str(filename))
            .run()
            )
    for filename in filenames:
        filename.unlink()

    # Serialization is trivial, as all policies are JSON serializable
    with open("file.json", "wt") as fp:
        fp.write(policy.json)

    with open("file.json", "rt") as fp:
        policy2 = DecisionTreePolicy.from_json("\n".join(fp))

    obs = observation_space.sample()
    assert policy(obs) == policy2(obs)
