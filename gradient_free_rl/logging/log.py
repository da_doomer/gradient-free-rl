"""A list of dictionaries for logging the progress of optimization
algorithms."""
from dataclasses import dataclass
from dataclasses import field
from typing import List
import json
import time


@dataclass
class Log:
    """A list of dictionaries."""
    log: List[dict] = field(default_factory=list)
    print_last_seconds: float = 0.0
    print_seconds_frequency: float = 2.0

    def push(self, statistics: dict, verbose=True):
        """Log the given dictionary of statistics."""
        seconds = time.time()
        seconds_since_print = seconds - self.print_last_seconds
        if verbose and seconds_since_print >= self.print_seconds_frequency:
            self.log.append(statistics)
            print(json.dumps(statistics, indent=4, default=str))
            self.print_last_seconds = seconds
