"""Logging operations related to pixel arrays."""
import math
from pathlib import Path
import uuid
from PIL import Image
import ffmpeg


def save_pixel_array(pixel_array: list[list[int]], filename: Path):
    """Save a given pixel array to an image using Pillow."""
    image = Image.fromarray(pixel_array)
    image.save(filename)


def pixel_arrays_to_video(
        pixel_arrays: list[list[list[int]]],
        filename: Path,
        fps: int,
        ):
    """Save a list of pixel arrays to a video using ffmpeg and Pillow."""
    filename = Path(filename)
    if filename.exists():
        filename.unlink()
    filenames = list()
    tempid = str(uuid.uuid4())[:5]
    for i, pixel_array in enumerate(pixel_arrays):
        idstr = tempid + str(i).rjust(
                int(math.log(len(pixel_arrays)))*2+1, "0"
            )
        array_filename = (filename.parent/idstr).with_suffix(".png")
        save_pixel_array(pixel_array, array_filename)
        filenames.append(array_filename)
    (
        ffmpeg
        .input(filename.parent/"*.png", pattern_type="glob", framerate=fps)
        .output(str(filename))
        .run()
    )
    for img_filename in filenames:
        img_filename.unlink()
