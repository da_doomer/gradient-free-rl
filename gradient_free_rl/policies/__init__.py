"""Mappings from observations to actions."""
from typing import Union
from .mlp import MLPPolicy
from .decision_tree import DecisionTreePolicy
from .constant import ConstantPolicy

Policy = Union[MLPPolicy, DecisionTreePolicy, ConstantPolicy]
EvolvablePolicy = Union[MLPPolicy, DecisionTreePolicy, ConstantPolicy]
