"""The class of policies defined by Multi Layer Perceptrons: sequences of
functions of the form

    f_i(x) = g(W_i x + b_i)

."""
# Native imports
from typing import List
from typing import Tuple
from typing import Optional
from typing import Union
from dataclasses import dataclass
from dataclasses import field
import copy

# External imports
import numpy as np
from numpy.random import default_rng
import jsons
from simple_nns.networks import MLP

# Local imports
from .. import spaces
from .evolvable import EvolvablePolicyABC


@dataclass
class MLPPolicy(EvolvablePolicyABC):
    """The classic multi-layer perceptron: a sequence of functions of the form

        f_i(x) = g(W_i x + b_i).

       The available activation functions are: ["relu", "tanh"].

       Parameters are initialized with a standard normal distribution with
       mean zero and per-layer standard deviation of `1/sqrt(in_features) *
       self.stdev`.
    """
    observation_space: Union[spaces.Box, spaces.Discrete]
    action_space: Union[spaces.Box, spaces.Discrete]
    mutation_stdev: float
    mlp: MLP

    @property
    def json(self) -> str:
        """The JSON string representation of a Network."""
        return jsons.dumps(self, strip_properties=True, jdkwargs=dict(indent=4))

    @staticmethod
    def from_json(json_: str) -> "MLPPolicy":
        """De-serialize the given serialized Network."""
        return jsons.loads(json_, MLPPolicy)

    def _processed_observation(self, observation):
        if isinstance(self.observation_space, spaces.Box):
            return np.array(observation)/np.linalg.norm(observation)
        if isinstance(self.observation_space, spaces.Discrete):
            _x = [0]*self.mlp.ins
            _x[observation] = 1
            return _x
        raise ValueError(
                f"Observation space {self.action_space} not supported"
            )

    def _processed_action(self, action):
        if isinstance(self.action_space, spaces.Box):
            return np.array(action).tolist()
        if isinstance(self.action_space, spaces.Discrete):
            return np.argmax(action)
        raise ValueError(
                f"Action space {self.action_space} not supported"
            )

    def __call__(self, observation):
        _x = self._processed_observation(observation)
        _y = self.mlp(_x)
        return self._processed_action(_y)

    def mutated(self) -> "MLPPolicy":
        """Return a mutated MLP obtained by adding to its parameters a vector
        sampled from the normal distribution."""
        new_policy = copy.deepcopy(self)
        new_layers = list()
        for W, b in self.mlp.layers:
            W2 = W + np.random.normal(size=np.array(W).shape)*self.mutation_stdev
            b2 = b + np.random.normal(size=np.array(b).shape)*self.mutation_stdev
            new_layers.append((W2, b2))
        new_policy.mlp = MLP(new_layers, new_policy.mlp.activation)
        return new_policy

    def crossover(self, other: "MLPPolicy") -> "MLPPolicy":
        """The crossover of p_1 and p_2."""
        new_policy = copy.deepcopy(self)
        new_layers = list()
        for (W1, b1), (W2, b2) in zip(self.mlp.layers, other.mlp.layers):
            W3 = (np.array(W1) + np.array(W2))/2.0
            b3 = (np.array(b1) + np.array(b2))/2.0
            new_layers.append((W3, b3))
        new_policy.mlp = MLP(new_layers, new_policy.mlp.activation)
        return new_policy

    @staticmethod
    def from_uniform(
            inner_sizes: list[int],
            activation: str,
            observation_space: Union[spaces.Box, spaces.Discrete],
            action_space: Union[spaces.Box, spaces.Discrete],
            mutation_stdev: float = 0.1,
            ):
        """Create an `MLPPolicy` with uniform initialization for each layer."""
        if isinstance(observation_space, spaces.Box):
            ins = len(observation_space.sample())
        elif isinstance(observation_space, spaces.Discrete):
            ins = observation_space.n
        else:
            raise ValueError(
                    f"Unsupported observation space {observation_space}"
                )
        if isinstance(action_space, spaces.Box):
            outs = len(action_space.sample())
        elif isinstance(action_space, spaces.Discrete):
            outs = action_space.n
        else:
            raise ValueError(
                    f"Action space {action_space} not supported"
                )
        mlp = MLP.from_uniform(
                ins=ins,
                outs=outs,
                inner_sizes=inner_sizes,
                activation=activation,
            )
        return MLPPolicy(
                observation_space=observation_space,
                action_space=action_space,
                mutation_stdev=mutation_stdev,
                mlp=mlp,
            )
