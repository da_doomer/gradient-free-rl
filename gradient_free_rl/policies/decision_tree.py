"""A binary decision tree with Policy objects as children."""
# Native imports
from typing import List
from typing import Tuple
from typing import Optional
from typing import Callable
from typing import Union
from dataclasses import dataclass
from dataclasses import field
from dataclasses import asdict
import copy
from enum import Enum
from enum import auto

# External imports
from numpy.random import default_rng
import jsons

# Local imports
from .evolvable import EvolvablePolicyABC
from ..predicates import Predicate
from .. import spaces
import gradient_free_rl.policies


class Mutations(Enum):
    """Available mutations for decision trees."""
    MUTATE_LEFT = auto()
    MUTATE_RIGHT = auto()
    EXTEND_LEFT = auto()
    EXTEND_RIGHT = auto()
    CONTRACT_LEFT = auto()
    CONTRACT_RIGHT = auto()
    MUTATE_PREDICATE = auto()


@dataclass
class DecisionTreePolicy(EvolvablePolicyABC):
    """A binary decision tree with Policy leaf nodes. Evolving
    a DecisionTreePolicy recursively adds DecisionTreePolicy children.
    """
    max_height: int
    predicate: Optional[Predicate] = None
    children: Optional[Tuple["gradient_free_rl.policies.EvolvablePolicy", "gradient_free_rl.policies.EvolvablePolicy"]] = None
    mutations = list(Mutations)
    predicate_maker: Optional[Callable[[], Predicate]] = field(repr=False, default=None)
    policy_maker: Optional[Callable[[], "gradient_free_rl.policies.EvolvablePolicy"]] = field(repr=False, default=None)

    def __post_init__(self):
        # Compute total number of parameters
        if self.children is None:
            self.children = (self.policy_maker(), self.policy_maker())
        if self.predicate is None:
            self.predicate = self.predicate_maker()

    @property
    def json(self) -> str:
        selfc = copy.deepcopy(self)
        selfc.predicate_maker = None
        selfc.policy_maker = None
        return jsons.dumps(selfc, jdkwargs=dict(indent=4))

    @staticmethod
    def from_json(json_: str) -> "DecisionTreePolicy":
        return jsons.loads(json_, DecisionTreePolicy)

    def __call__(self, observation) -> List[float]:
        if self.predicate(observation):
            return self.children[0](observation)
        return self.children[1](observation)

    def mutated(self) -> "DecisionTreePolicy":
        rng = default_rng()
        mutation = rng.choice(self.mutations)
        new_tree = copy.deepcopy(self)
        if mutation is Mutations.MUTATE_LEFT:
            children = (self.children[0].mutated(), self.children[1])
            new_tree.children = children
            return new_tree
        if mutation is Mutations.MUTATE_RIGHT:
            children = (self.children[0], self.children[1].mutated())
            new_tree.children = children
            return new_tree
        if mutation is Mutations.EXTEND_LEFT:
            left = self.mutated()
            children = (left, self.children[1])
            new_tree.children = children
            return new_tree
        if mutation is Mutations.EXTEND_RIGHT:
            right = self.mutated()
            children = (self.children[0], right)
            new_tree.children = children
            return new_tree
        if mutation is Mutations.CONTRACT_LEFT:
            left = self.policy_maker()
            children = (left, self.children[1])
            new_tree.children = children
            return new_tree
        if mutation is Mutations.CONTRACT_RIGHT:
            right = self.policy_maker()
            children = (self.children[0], right)
            new_tree.children = children
            return new_tree
        if mutation is Mutations.MUTATE_PREDICATE:
            predicate = self.predicate_maker()
            new_tree.predicate = predicate
            return new_tree
        raise ValueError(
                f"Mutation {mutation} is not supported"
            )

    def crossover(self, other: "DecisionTreePolicy") -> "DecisionTreePolicy":
        # TODO
        pass
