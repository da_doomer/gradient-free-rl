"""An abstract base class for all policy classes with "crossover" and "mutated"
methods."""
from abc import abstractmethod
from .policy import PolicyABC


class EvolvablePolicyABC(PolicyABC):
    """A policy class is evolvable if its policies can be crossover-ed and
    mutated."""

    @abstractmethod
    def crossover(self, other: "EvolvablePolicy") -> "EvolvablePolicy":
        """The crossover of `self` and `other`."""

    @abstractmethod
    def mutated(self) -> "EvolvablePolicy":
        """A mutated version of `self`."""
