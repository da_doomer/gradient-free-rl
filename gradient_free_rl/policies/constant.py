"""The class of constant policies.
"""
# Native imports
from typing import List
from typing import Union
from dataclasses import dataclass
from dataclasses import asdict
import copy

# External imports
import numpy as np
from numpy.random import default_rng
import jsons

# Local imports
from .evolvable import EvolvablePolicyABC
from ..types import Action
from ..types import Observation
from .. import spaces


@dataclass
class ConstantPolicy(EvolvablePolicyABC):
    """A constant function.

       When `action` is not `None`, if `action_space` is `spaces.Box` then
       parameters are initialized with a standard normal distribution with mean
       zero and per-layer standard deviation of `stdev`, else `action`
       is chosen with a uniform distribution over the `spaces.Discrete`.
    """
    action_space: Union[spaces.Box, spaces.Discrete]
    action = None
    stdev: float = 0.1

    def __post_init__(self):
        # Compute total number of parameters
        if self.action is None:
            self.action = self.action_space.sample()

    @property
    def json(self) -> str:
        return jsons.dumps(self, strip_properties=True, jdkwargs=dict(indent=4))

    @staticmethod
    def from_json(json_: str) -> "ConstantPolicy":
        return jsons.loads(json_, ConstantPolicy)

    def __call__(self, observation: Observation) -> Action:
        return self.action

    def mutated(self) -> "ConstantPolicy":
        rng = default_rng()
        new = copy.deepcopy(self)
        if isinstance(self.action_space, spaces.Box):
            new.action = self.action + rng.normal(size=(len(self.action),))*self.stdev
            return new
        if isinstance(self.action_space, spaces.Discrete):
            new.action = rng.choice(range(self.action_space.n))
            return new
        raise ValueError(
                f"Action space {self.action_space} not supported"
            )

    def crossover(self, other: "ConstantPolicy") -> "ConstantPolicy":
        rng = default_rng()
        new = copy.deepcopy(self)
        if isinstance(self.action_space, spaces.Box):
            new.action = (np.array(self.action) + np.array(other.action))/2.0
            return new
        if isinstance(self.action_space, spaces.Discrete):
            new.action = rng.choice(self.action, other.action)
            return new
        raise ValueError(
                f"Action space {self.action_space} not supported"
            )
