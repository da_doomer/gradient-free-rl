"""Main Abstract Base Class for policies."""
# Native imports
from abc import ABC
from abc import abstractmethod
from ..types import Action
from ..types import Observation


class PolicyABC(ABC):
    """A policy is a callable JSON-serializable object."""
    @property
    @abstractmethod
    def json(self) -> str:
        """The JSON string representation of the policy."""

    @staticmethod
    @abstractmethod
    def from_json(json_: str) -> "Policy":
        """De-serialize the given serialized policy."""

    @abstractmethod
    def __call__(self, observation: Observation) -> Action:
        """Return the action conditioned on the given observation."""
