"""A threshold-based predicate."""
from dataclasses import dataclass
from typing import Optional
from typing import Union
import copy
from numpy.random import default_rng
import jsons
from .predicate import PredicateABC
from .. import spaces


@dataclass
class ThresholdPredicate(PredicateABC):
    """A threshold-based predicate."""
    observation_space: Union[spaces.Box, spaces.Discrete]
    threshold: Union[float, int] = 0
    stdev: Optional[float] = None
    index: Optional[int] = 0

    def _value(self, observation) -> float:
        if isinstance(self.observation_space, spaces.Box):
            return observation[self.index]
        if isinstance(self.observation_space, spaces.Discrete):
            return observation
        raise ValueError(
                f"Observation space {self.observation_space} not supported"
            )

    def __call__(self, observation) -> bool:
        return self._value(observation) < self.threshold

    def mutated(self) -> "ThresholdPredicate":
        """Return a mutated ThresholdPredicate."""
        rng = default_rng()
        if isinstance(self.observation_space, spaces.Box):
            new_predicate = copy.deepcopy(self)
            new_threshold = self.threshold + rng.normal()*self.stdev
            new_index = rng.choice(range(len(self.observation_space.sample())))
            new_predicate.threshold = new_threshold
            new_predicate.index = new_index
            return new_predicate
        if isinstance(self.observation_space, spaces.Discrete):
            new_predicate = copy.deepcopy(self)
            new_threshold = rng.choice(range(self.observation_space.n))
            new_predicate.threshold = new_threshold
            return new_predicate
        raise ValueError(
                f"Observation space {self.observation_space} not supported"
            )

    def crossover(self, other: "ThresholdPredicate") -> "ThresholdPredicate":
        """The crossover of p_1 and p_2."""
        # TODO

    @property
    def json(self) -> str:
        return jsons.dumps(self, strip_properties=True, jdkwargs=dict(indent=4))

    @staticmethod
    def from_json(json_: str) -> "ThresholdPredicate":
        return jsons.loads(json_, ThresholdPredicate)
