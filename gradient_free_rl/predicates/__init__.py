"""Mappings from observations to boolean values."""
from typing import Union
from .threshold import ThresholdPredicate

Predicate = Union[ThresholdPredicate]
