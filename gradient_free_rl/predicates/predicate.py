"""Mappings from observations to boolean values."""
from abc import ABC
from abc import abstractmethod
from ..types import Observation


class PredicateABC(ABC):
    """A predicate is a callable JSON-serializable object mapping observations
    to boolean values."""
    @property
    @abstractmethod
    def json(self) -> str:
        """The JSON string representation of the policy."""

    @staticmethod
    @abstractmethod
    def from_json(json_: str) -> "Predicate":
        """De-serialize the given serialized policy."""

    @abstractmethod
    def __call__(self, observation: Observation) -> bool:
        """Return the action conditioned on the given observation."""
