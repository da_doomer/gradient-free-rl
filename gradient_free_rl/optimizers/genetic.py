"""A simple genetic algorithm for all Evolvable policy classes."""
from typing import Callable
from typing import Tuple
from typing import Optional
from dataclasses import dataclass
from dataclasses import field
from dataclasses import asdict
import json
import concurrent.futures
import os
import random
import time
import statistics
import copy
from .optimizer import Optimizer
import gym
from ..policies import EvolvablePolicy


def evaluate(
        policy: Callable,
        env_maker: Callable[[], gym.Env],
        timesteps: int,
        eval_n: int,
        ) -> Tuple[float, int]:
    """Receive instructions to evaluate the given MLP in the given
    environment over the given queue."""
    total_reward = 0.0
    total_steps = 0
    for _ in range(eval_n):
        env = env_maker()
        obs = env.reset()
        for _ in range(timesteps):
            action = policy(obs)
            obs, reward, done, _ = env.step(action)
            total_reward += reward
            total_steps += 1
            if done:
                break
    return total_reward/eval_n, total_steps


@dataclass
class GeneticOptimizer(Optimizer):
    """An optimizer implementing a simple genetic algorithm for multi layer
    perceptrons."""
    elite_n: int = 1
    crossover_n: int = 32
    mutated_n: int = 32
    max_workers: Optional[int] = field(default_factory=os.cpu_count)
    rollout_len: int = 32
    rollout_n: int = 1
    generation_i: int = 0
    mp_context: Optional = None

    def optimized(
            self,
            policy: EvolvablePolicy,
            timesteps: int,
            verbose: bool = True
            ) -> EvolvablePolicy:
        """Optimize the underlying policy for approximately the given
        number of timesteps."""
        if timesteps == 0:
            return copy.deepcopy(policy)

        population = [
                policy.mutated()
                for _ in range(self.mutated_n)
            ]
        initial_timesteps = self.total_timesteps
        learn_start_seconds = time.time()
        with concurrent.futures.ProcessPoolExecutor(
                max_workers=self.max_workers,
                mp_context=self.mp_context,
                ) as executor:
            while self.total_timesteps < initial_timesteps + timesteps:
                # Track generation statistics
                start_seconds = time.time()
                generation_rewards = list()
                generation_timesteps = list()

                # Compute reward for each policy
                futures = [
                        executor.submit(
                            evaluate,
                            policy=mlp,
                            env_maker=self.make_env,
                            timesteps=self.rollout_len,
                            eval_n=self.rollout_n,
                        )
                        for mlp in population
                    ]
                for future in futures:
                    total_reward, performed_timesteps = future.result()
                    generation_timesteps.append(performed_timesteps)
                    generation_rewards.append(total_reward)

                # Sort policies by reward and set the best policy as current
                # policy
                population_order = list(reversed(sorted(range(len(population)), key=lambda i: generation_rewards[i])))
                policy = population[population_order[0]]
                self.generation_i += 1
                self.total_timesteps += sum(generation_timesteps)

                # Build a collection of policies mutating the current policy
                elites = [
                        population[i]
                        for i in population_order[:self.elite_n]
                    ]
                population = list()
                population += [
                        elite.mutated()
                        for elite in random.choices(elites, k=self.mutated_n)
                    ]
                population += [
                        random.choice(elites).crossover(random.choice(elites))
                        for _ in range(self.crossover_n)
                    ]
                population += elites

                # Log statistics
                current_seconds = time.time()
                generation_seconds = current_seconds - start_seconds
                timesteps_per_second = int(sum(generation_timesteps)/generation_seconds)
                seconds_since_learn_start = int(current_seconds - learn_start_seconds)
                self.log.push(dict(
                    total_timesteps=self.total_timesteps,
                    rollout_reward_max=max(generation_rewards),
                    rollout_reward_stdev=statistics.stdev(generation_rewards),
                    timesteps_per_second=timesteps_per_second,
                    generation=self.generation_i,
                    seconds_since_learn_start=seconds_since_learn_start,
                    rollout_mean_timesteps=statistics.mean(generation_timesteps)
                ), verbose=verbose)
        return policy

    @property
    def json(self) -> str:
        """The JSON string representation of the optimizer."""
        selfd = asdict(self)
        selfd["make_env"] = None
        return json.dumps(selfd)

    @staticmethod
    def from_json(json_: str) -> "GeneticOptimizer":
        """De-serialize the given serialized optimizer."""
        d = json.loads(json_)
        return GeneticOptimizer(**d)
