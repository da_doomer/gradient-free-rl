"""Following the style of the stable-baselines API, an Optimizer
is an object-oriented way of defining optimization algorithms for
OpenAI Gym environments."""
from typing import Callable
from abc import abstractmethod
from abc import ABC
from dataclasses import dataclass
from dataclasses import field
import gym
from ..policies import Policy
from ..logging import Log


@dataclass
class Optimizer(ABC):
    """Generic interface for Optimizers."""
    make_env: Callable[[], gym.Env] = field(repr=False)
    total_timesteps: int = 0
    log: Log = field(repr=False, default_factory=Log)

    @abstractmethod
    def optimized(
            self,
            policy: Policy,
            timesteps: int,
            verbose: bool = True
            ) -> Policy:
        """Optimize the underlying policy for approximately the given
        number of timesteps."""

    @property
    @abstractmethod
    def json(self) -> str:
        """The JSON string representation of a the Optimizer."""

    @staticmethod
    @abstractmethod
    def from_json(json_: str) -> "Optimizer":
        """De-serialize the given serialized Optimizer."""
