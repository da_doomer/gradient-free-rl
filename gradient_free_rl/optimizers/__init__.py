"""Object-oriented implementations of gradient-free optimization algorithms."""
from .genetic import GeneticOptimizer
