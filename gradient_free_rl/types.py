"""Primitive types."""
from typing import List
from typing import Union

Observation = Union[List[float], int]
Action = Union[List[float], int]
