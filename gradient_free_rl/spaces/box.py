"""A continuous vector space."""
from dataclasses import dataclass
from dataclasses import field
from typing import List
from numpy.random import default_rng
from .space import Space


@dataclass
class Box(Space):
    """A continuous vector space."""
    n: int
    low: List[float] = field(repr=False)
    high: List[float] = field(repr=False)

    def sample(self) -> List[float]:
        rng = default_rng()
        return rng.normal(size=(self.n,)).tolist()
