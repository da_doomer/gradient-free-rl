"""A finite space."""
from dataclasses import dataclass
from typing import List
from numpy.random import default_rng
from .space import Space


@dataclass
class Discrete(Space):
    """A finite space indexed with integers."""
    n: int

    def sample(self) -> List[float]:
        rng = default_rng()
        return rng.choice(range(self.n))
