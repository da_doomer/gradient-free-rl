"""Minimal pure-python implementation of some Open AI Gym spaces."""
from .space import Space
from .box import Box
from .discrete import Discrete
