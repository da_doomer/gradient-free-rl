"""Pure-python facade for some OpenAI Gym spaces."""
from abc import ABC
from abc import abstractmethod
import gym


class Space(ABC):
    """Interface for all Spaces."""
    @staticmethod
    def from_gym(space: gym.Space) -> "Space":
        """Instantiate a Space using an OpenAI Gym space."""
        from .box import Box
        from .discrete import Discrete
        if isinstance(space, gym.spaces.Box):
            return Box(len(space.sample()), space.low.tolist(), space.high.tolist())
        if isinstance(space, gym.spaces.Discrete):
            return Discrete(space)
        raise ValueError(
                f"Space {space} not supported"
            )

    @abstractmethod
    def sample(self):
        """Sample from this space."""
