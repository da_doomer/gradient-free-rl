# gradient-free-rl

A collection of gradient free algorithms for optimizing policies in Markov
Decision Processes.

## Installation

Use your favorite package manager. With pip this is:

```bash
python -m pip install git+https://gitlab.com/da_doomer/gradient-free-rl.git
```

## Documentation

See [https://da_doomer.gitlab.io/gradient-free-rl/](https://da_doomer.gitlab.io/gradient-free-rl/).
