{{ name }}
{{ underline }}

.. autoclass:: {{ fullname }}
  :members:
  :show-inheritance:
  :inherited-members:
