Welcome to gradient-free-rl
====================================

gradient-free-rl is a collection of gradient-free optimization algorithms for
Reinforcement Learning.

Documentation:

.. autosummary::
   :toctree: _autosummary

   gradient_free_rl.policies
   gradient_free_rl.predicates
   gradient_free_rl.optimizers
   gradient_free_rl.logging


Quickstart
----------

``$ python -m pip install --user git+https://gitlab.com/da_doomer/gradient-free-rl.git``

.. literalinclude:: ../examples/genetic_mlp.py

Source code
-----------

* https://gitlab.com/da_doomer/gradient-free-rl

Documentation
-------------

* https://da_doomer.gitlab.io/gradient-free-rl/

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
