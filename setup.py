#!/usr/bin/env python3

from setuptools import setup

setup(
    name='gradient_free_rl',
    version='0.1',
    install_requires=[
        'numpy',
        'gym',
        'jsons',
        'simple_nns@git+https://gitlab.com/da_doomer/simple-nns.git',
    ],
    description='Gradient-free optimization algorithms for reinforcement learning',
    packages=['gradient_free_rl', 'gradient_free_rl.optimizers'],
    author='da_doomer',
    license='MIT',
    platforms='Linux; Windows; OS X'
    )
